import 'package:app_uv_doctor2/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:app_uv_doctor2/screens/singin_screen.dart';
import 'package:app_uv_doctor2/screens/request_screen.dart';
import 'package:app_uv_doctor2/screens/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('print main');
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: RequestScreen.id,
      routes: {
        // SignupScreen.id: (context) => SignupScreen(),
        SplashScreen.id: (context) => SplashScreen(),
        HomeScreen.id: (context) => HomeScreen(),
        RequestScreen.id: (context) => RequestScreen(),
      },
    );
  }
}
