import 'dart:convert';

class Data {
  String folderId;
  String title;
  String bodyArea;
  String skinColor;
  String doctorId;
  String diseases;
  String undelyingDiseases;
  String active;
  String countOfSession;
  String startTime;
  String duration;
  String creatTime;

  Data(
      {this.folderId,
      this.title,
      this.bodyArea,
      this.skinColor,
      this.doctorId,
      this.diseases,
      this.undelyingDiseases,
      this.active,
      this.countOfSession,
      this.startTime,
      this.duration,
      this.creatTime});

  Data.fromJson(Map<String, String> json) {
    folderId = json['folder_id'];
    title = json['title'];
    bodyArea = json['BodyArea'];
    skinColor = json['SkinColor'];
    doctorId = json['doctor_id'];
    diseases = json['diseases'];
    undelyingDiseases = json['UndelyingDiseases'];
    active = json['active'];
    countOfSession = json['count_of_session'];
    startTime = json['start_time'];
    duration = json['duration'];
    creatTime = json['CreatTime'];
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['folder_id'] = this.folderId;
    data['title'] = this.title;
    data['BodyArea'] = this.bodyArea;
    data['SkinColor'] = this.skinColor;
    data['doctor_id'] = this.doctorId;
    data['diseases'] = this.diseases;
    data['UndelyingDiseases'] = this.undelyingDiseases;
    data['active'] = this.active;
    data['count_of_session'] = this.countOfSession;
    data['start_time'] = this.startTime;
    data['duration'] = this.duration;
    data['CreatTime'] = this.creatTime;
    return data;
  }
}
