import 'package:app_uv_doctor2/model/fetchmodel.dart';
import 'package:app_uv_doctor2/shared/shared.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class Fetch {
  Future<List<Data>> fetch() async {
    var url = Uri.parse('http://uv.mandoon.ir/doctor/folder/index.php');
    String token;
    token = Shared().getvalue(Shared.token);
    var jsonresponse;
    var response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }).timeout(Duration(seconds: 10));
    print(response.body);
    print('Token : ${token}');
    print(response);
    jsonresponse = jsonDecode(response.body);

    print('st: ${response.statusCode}');
    List<Data> datas = [];

    for (var singledata in jsonresponse) {
      Data data = Data(
        folderId: singledata["folderId"],
        title: singledata["title"],
        bodyArea: singledata["bodyArea"],
        skinColor: singledata["skinColor"],
        doctorId: singledata["doctorId"],
        diseases: singledata["diseases"],
        undelyingDiseases: singledata["undelyingDiseases"],
        active: singledata["active"],
        countOfSession: singledata["countOfSession"],
        startTime: singledata["startTime"],
        duration: singledata["duration"],
        creatTime: singledata["creatTime"],
      );
      datas.add(data);

      //return
      //postFromJson(jsonresponse);
      //Data.fromJson(jsonresponse);
    }
    return datas;
  }
}
