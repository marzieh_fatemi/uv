import 'package:app_uv_doctor2/screens/myrequest_screen.dart';
import 'package:flutter/material.dart';

import 'package:app_uv_doctor2/screens/request_screen.dart';

class HomeScreen extends StatefulWidget {
  static String id = 'home_screen';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('UV_Doctor'),
        automaticallyImplyLeading: false,
        bottom: TabBar(
          controller: _tabController,
          tabs: [
            Tab(
              text: 'Request',
            ),
            Tab(
              text: 'My Request',
            )
          ],
        ),
      ),
      body: TabBarView(
        children: [
          RequestScreen(),
          MyRequst(),
        ],
        controller: _tabController,
      ),
    );
  }
}
