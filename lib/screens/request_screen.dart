import 'package:app_uv_doctor2/network/login.dart';
import 'package:app_uv_doctor2/network/requestfetch.dart';
import 'package:app_uv_doctor2/model/fetchmodel.dart';
import 'package:app_uv_doctor2/wiggets/request_screen_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:app_uv_doctor2/screens/request_screen.dart';
import 'dart:convert';
import 'dart:async';
import 'package:app_uv_doctor2/shared/shared.dart';

class RequestScreen extends StatefulWidget {
  static String id = 'request_screen';
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  Future<Data> _data;
  // Future mm;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _data = fetchdata();

    print('request view');
    /* mm = Request().fetch().then((value) {
      print('----- ${value.length}');
    }).catchError((onError) {
      print('catch error $onError');
    });*/
  }

  Future<Data> fetchdata() async {
    print('fetch data in request');
    var url = Uri.parse('http://uv.mandoon.ir/doctor/folder/index.php');
    String token;
    token = Shared().getvalue(Shared.token);
    final response = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }).timeout(Duration(seconds: 10));

    print('response: ${response}');

    if (response.statusCode == 200) {
      return Data.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load post');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: FutureBuilder(
          future: fetchdata().then((value) {
            print('----- ${value}');
          }).catchError((onError) {
            print('catch error $onError');
          }),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.all(12.0),
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        elevation: 3,
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(
                              vertical: 18.0, horizontal: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DataWidget(
                                title: 'title',
                                data: snapshot.data['title'],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Divider(),
                              DataWidget(
                                title: 'name:',
                                data: 'k',
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Divider(),
                              DataWidget(
                                title: 'body_area:',
                                data: 'k',
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Divider(),
                              DataWidget(
                                title: 'Time:',
                                data: 'k',
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  });
            } else {
              return CircularProgressIndicator();
            }
          }),
    );
  }
}
