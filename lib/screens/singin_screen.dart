import 'package:app_uv_doctor2/screens/home_screen.dart';
import 'package:app_uv_doctor2/shared/shared.dart';
import 'package:flutter/material.dart';
import 'package:app_uv_doctor2/wiggets/signin_widget.dart';
import 'package:app_uv_doctor2/network/login.dart';

class SignupScreen extends StatefulWidget {
  static String id = 'signup_screen';

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  bool _isloading = false;
  TextEditingController _controllername = TextEditingController();
  TextEditingController _controllerpass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder(
          future: LogIn().signIn(_controllername.text, _controllerpass.text),
          builder: (context, snapshot) {
            return SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      width: 70.0,
                      height: 70.0,
                      fit: BoxFit.fill,
                      image: AssetImage('images/b.png'),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        'UV_Doctor',
                        style: TextStyle(fontWeight: FontWeight.w900),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    TextFormWidget(
                      textEditingController: _controllername,
                      textInputType: TextInputType.name,
                      hintText: 'user',
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    TextFormWidget(
                      textEditingController: _controllerpass,
                      textInputType: TextInputType.visiblePassword,
                      hintText: 'password',
                    ),
                    SizedBox(
                      height: 22.0,
                    ),
                    Container(
                      width: double.infinity,
                      height: 50.0,
                      child: ElevatedButton(
                        onPressed: () async {
                          if (_controllername.text == "" ||
                              _controllerpass.text == "") {
                            return;
                          } else {
                            setState(() {
                              _isloading = true;
                            });

                            await LogIn()
                                .signIn(
                                    _controllername.text, _controllerpass.text)
                                .then((value) {
                              setState(() {
                                _isloading = false;
                              });

                              if (value.containsKey('data')) {
                                Shared().setvalue(
                                    Shared.token, value['data']['token']);
                                Navigator.pushNamed(context, HomeScreen.id);
                              } else {
                                final snackbar = SnackBar(
                                    content: Text(
                                        'Please enter the values correctly '));
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackbar);
                              }
                            }).catchError((onError) {
                              setState(() {
                                _isloading = false;
                              });
                              print('onerror: ${onError}');
                              final snackbar = SnackBar(content: Text(onError));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackbar);
                            });
                          }
                        },
                        child: _isloading
                            ? CircularProgressIndicator(
                                backgroundColor: Colors.grey,
                              )
                            : Text('login'),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
