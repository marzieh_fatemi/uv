import 'dart:async';

//import 'package:app_uv_doctor/screens/signup_screen.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  static String id = 'splash_screen';
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    /* Timer(
      Duration(seconds: 4),
      () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SignupScreen()),
      ),
    );*/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          color: Colors.blueAccent,
        ),
        Container(
          padding: EdgeInsets.only(bottom: 40.0),
          alignment: Alignment.bottomCenter,
          child: CircularProgressIndicator(),
        )
      ],
    ));
  }
}
