import 'package:shared_preferences/shared_preferences.dart';

class Shared {
  static const String token = "token";

  setvalue(key, value) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(key, value);
  }

  getvalue(String key) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.getString(key);
  }
}
