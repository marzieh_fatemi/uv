import 'package:flutter/material.dart';

class DataWidget extends StatelessWidget {
  final String title;
  final String data;
  DataWidget({this.data, this.title});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Text(data)
        ],
      ),
    );
  }
}
